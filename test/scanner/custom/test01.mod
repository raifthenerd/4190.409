// custom test set: details on our report.


// expecting tUndefined (\'\')
''

// expecting tUndefined (\")
"
// expecting tUndefined (\"hello)
"hello
// expecting tUndefined (\"hello), tIdent (world), tUndefined (\")
"hello
world"

// expecting tUndefined (\')
'
// expecting tUndefined (\'h)
'h
// expecting tUndefined (\'), tUndefined (\')
'
'
// expecting tUndefined (\'h), tIdent (ello), tUndefined (\')
'hello'

// expecting tUndefined (\'\\h\')
'\h'
// expecting tUndefined (\"\\hello\")
"\hello"

// expecting tUndefined (\'\\h)
'\h
// expecting tUndefined (\"\\hello)
"\hello

// expecting tIf, tParL
if(// comment)

// expecting tNum (0), tIdent (_0__00)
0_0__00

// expecting tStr (""), tUndefined (\")
"""
// expecting tStr ("\'")
"'"
// expecting tChar ('\"')
'"'
// expecting tUndefined (\'\'), tUndefined (\')
'''
// expecting tStr ("\"")
"\""
// expecting tChar ('\'')
'\''
