module M;

var A: char[5];

procedure bar(x: integer);
begin
end bar;

procedure foo(a: char[5]);
var b: char[5];
begin
  a[1] := 1;
  A[2] := 2;
  bar(a);
  bar(A)
end foo;

begin
end M.
