#!/bin/bash

PHASE="$1"
SUCCESS=0
TOTAL=0
if [ "$#" -eq 0 ]; then
    echo "usage: $0 [PHASE] ([TESTSET]...)"
    exit 1
elif [ "$#" -eq 1 ]; then
    TESTS=(test/"$PHASE"/*.mod test/"$PHASE"/**/*.mod)
else
    TESTS="${@:2}"
fi
for f in ${TESTS[@]}; do
    echo "testing $f"
    if [ ! -f "$f" ]; then
        echo "... the test file doesn't exist"
        exit 1
    fi
    ((TOTAL++))
    RESULT="${f%.mod}.expect.txt"
#        sed -i -r -e 's/#.*$//g' -e '/^ *$/d' "$f".s
    if [ "$PHASE" = "snuplc" ]; then
        rm -f "$f".s "$f".my.txt "$f".ref.txt
        ./snuplc --exe "$f" > "$f".my.txt 2>&1
        if [ -f "$f".s ]; then
            cat "$f".s >> "$f".my.txt
            rm -f "$f".s
        fi
        ./ref/snuplc --exe "$f" > "$f".ref.txt 2>&1
        if [ -f "$f".s ]; then
            cat "$f".s >> "$f".ref.txt
            rm -f "$f".s
        fi
        if [ -f "$RESULT" ]; then
            diff <(diff "$f".my.txt "$f".ref.txt) \
                 $RESULT \
                && ((SUCCESS++))
        else
            diff "$f".my.txt "$f".ref.txt \
                && ((SUCCESS++))
        fi
    else
        if [ -f "$RESULT" ]; then
            diff <(diff <(./test_$PHASE $f) <(./ref/test_$PHASE $f)) \
                 $RESULT \
                && ((SUCCESS++))
        else
            diff <(./test_$PHASE $f) <(./ref/test_$PHASE $f) \
                && ((SUCCESS++))
        fi
    fi
done
echo "$PHASE test result: $SUCCESS of $TOTAL tests were succeeded."
if [[ $SUCCESS -ne $TOTAL ]]; then
    exit 1
fi
