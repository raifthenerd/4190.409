//------------------------------------------------------------------------------
/// @brief SnuPL/0 parser
/// @author Bernhard Egger <bernhard@csap.snu.ac.kr>
/// @section changelog Change Log
/// 2012/09/14 Bernhard Egger created
/// 2013/03/07 Bernhard Egger adapted to SnuPL/0
/// 2014/11/04 Bernhard Egger maintain unary '+' signs in the AST
/// 2016/04/01 Bernhard Egger adapted to SnuPL/1 (this is not a joke)
/// 2016/09/28 Bernhard Egger assignment 2: parser for SnuPL/-1
///
/// @section license_section License
/// Copyright (c) 2012-2016, Bernhard Egger
/// All rights reserved.
///
/// Redistribution and use in source and binary forms,  with or without modifi-
/// cation, are permitted provided that the following conditions are met:
///
/// - Redistributions of source code must retain the above copyright notice,
///   this list of conditions and the following disclaimer.
/// - Redistributions in binary form must reproduce the above copyright notice,
///   this list of conditions and the following disclaimer in the documentation
///   and/or other materials provided with the distribution.
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
/// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
/// IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
/// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER  OR CONTRIBUTORS BE
/// LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSE-
/// QUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF  SUBSTITUTE
/// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
/// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT
/// LIABILITY, OR TORT  (INCLUDING NEGLIGENCE OR OTHERWISE)  ARISING IN ANY WAY
/// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
/// DAMAGE.
//------------------------------------------------------------------------------

#include <limits.h>
#include <cassert>
#include <errno.h>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <exception>

#include "parser.h"
using namespace std;

//------------------------------------------------------------------------------
// CParser
//
CParser::CParser(CScanner *scanner)
{
  _scanner = scanner;
  _module = NULL;
}

CAstNode* CParser::Parse(void)
{
  _abort = false;

  if (_module != NULL) { delete _module; _module = NULL; }

  try {
    if (_scanner != NULL) _module = module();

    if (_module != NULL) {
      CToken t;
      string msg;
      if (!_module->TypeCheck(&t, &msg)) SetError(t, msg);
    }
  } catch (...) {
    _module = NULL;
  }

  return _module;
}

const CToken* CParser::GetErrorToken(void) const
{
  if (_abort) return &_error_token;
  else return NULL;
}

string CParser::GetErrorMessage(void) const
{
  if (_abort) return _message;
  else return "";
}

void CParser::SetError(CToken t, const string message)
{
  _error_token = t;
  _message = message;
  _abort = true;
  throw message;
}

bool CParser::Consume(EToken type, CToken *token)
{
  if (_abort) return false;

  CToken t = _scanner->Get();

  if (t.GetType() != type) {
    SetError(t, "expected '" + CToken::Name(type) + "', got '" +
             t.GetName() + "'");
  }

  if (token != NULL) *token = t;

  return t.GetType() == type;
}

void CParser::InitSymbolTable(CSymtab *s)
{
  // add predefined functions inside symbol table
  CSymProc* sym;
  CTypeManager *tm = CTypeManager::Get();
  const CType* strType =
    tm->GetPointer(tm->GetArray(CArrayType::OPEN, tm->GetChar()));

  // DIM: ptr, int -> int
  sym = new CSymProc("DIM", tm->GetInt());
  sym->AddParam(new CSymParam(0, "array", tm->GetVoidPtr()));
  sym->AddParam(new CSymParam(1, "dim", tm->GetInt()));
  s->AddSymbol(sym);
  // DOFS: ptr -> int
  sym = new CSymProc("DOFS", tm->GetInt());
  sym->AddParam(new CSymParam(0, "array", tm->GetVoidPtr()));
  s->AddSymbol(sym);
  // ReadInt: void -> int
  s->AddSymbol(new CSymProc("ReadInt", tm->GetInt()));
  // WriteInt: int -> void
  sym = new CSymProc("WriteInt", tm->GetNull());
  sym->AddParam(new CSymParam(0, "i", tm->GetInt()));
  s->AddSymbol(sym);
  // WriteChar: char -> void
  sym = new CSymProc("WriteChar", tm->GetNull());
  sym->AddParam(new CSymParam(0, "c", tm->GetChar()));
  s->AddSymbol(sym);
  // WriteStr: array char -> void
  sym = new CSymProc("WriteStr", tm->GetNull());
  sym->AddParam(new CSymParam(0, "str", strType));
  s->AddSymbol(sym);
  // WriteLn: void -> void
  s->AddSymbol(new CSymProc("WriteLn", tm->GetNull()));
}

CAstModule* CParser::module(void)
{
  //
  // module ::=
  // "module" ident ";" varDeclaration { subroutineDeclaration }
  // "begin" statSequence "end" ident ".".
  //
  // FIRST(subroutineDeclaration)
  // = { tProcedure, tFunction }
  //
  CAstModule *m;
  vector<CSymbol*> vars;
  CAstStatement *statseq;
  CToken t1, t2, t3;
  Consume(tModule, &t1);
  Consume(tIdent, &t2);
  Consume(tSemicolon);
  m = new CAstModule(t1, t2.GetValue());
  InitSymbolTable(m->GetSymbolTable());

  // declarations (global variables, subroutines)
  varDeclaration(m);
  EToken tt = _scanner->Peek().GetType();
  while (tt == tProcedure || tt == tFunction) {
    subroutineDeclaration(m);
    tt = _scanner->Peek().GetType();
  }

  // main body
  Consume(tBegin);
  m->SetStatementSequence(statSequence(m));
  Consume(tEnd);
  Consume(tIdent, &t3);
  Consume(tDot);
  if (t2.GetValue() != t3.GetValue())
    SetError(t3, "module identifier mismatch ('" +
             t2.GetValue() + "' != '" + t3.GetValue() + "').");
  return m;
}

CAstType* CParser::type(bool isParam)
{
  //
  // type ::=
  // basetype { "[" [ tNumber ] "]" }.
  //
  // basetype ::=
  // "boolean" | "char" | "integer".
  //
  // FOLLOW(type)
  // = { tSemicolon, tRParens }
  //
  CToken t;
  CTypeManager *tm = CTypeManager::Get();
  const CType *tp = NULL;

  // basetype
  Consume(_scanner->Peek().GetType(), &t);
  switch (t.GetType()) {
    case tBoolean:
      tp = tm->GetBool();
      break;
    case tChar:
      tp = tm->GetChar();
      break;
    case tInteger:
      tp = tm->GetInt();
      break;
    default:
      SetError(t, "basetype expected.");
      break;
  }
  // check whether array or not - then parse
  vector<int> sizes;
  CAstConstant *num = NULL;
  while(_scanner->Peek().GetType() == tLBrak) {
    Consume(tLBrak);
    int n = CArrayType::OPEN;
    if (!isParam || _scanner->Peek().GetType() == tNumber) {
      num = constNumber();
      n = num->GetValue();
      if (n <= 0)
        SetError(num->GetToken(), "positive constant expected.");
    }
    Consume(tRBrak);
    sizes.push_back(n);
  }
  for (auto it = sizes.rbegin(); it != sizes.rend(); ++it)
    tp = tm->GetArray(*it, tp);
  return new CAstType(t, tp);
}

int CParser::varDecl(CAstScope *s, bool isParam, int idx)
{
  //
  // varDecl ::=
  // ident { "," ident } ":" type.
  //
  CTypeManager *tm = CTypeManager::Get();
  CSymProc *sp = NULL;
  if (isParam) sp = dynamic_cast<CAstProcedure*>(s)->GetSymbol();
  // parse symbols & push into symbol table (and function paramters)
  vector<CSymbol*> vars;
  CToken t;
  int n = 0;
  while (true) {
    Consume(tIdent, &t);
    CSymbol *var = NULL;
    // currently we have no type info,
    // so make symbol with temporary null type and fix later
    if (isParam)
      var = new CSymParam(idx + n++, t.GetValue(), tm->GetNull());
    else
      var = s->CreateVar(t.GetValue(), tm->GetNull());
    if (!s->GetSymbolTable()->AddSymbol(var))
      SetError(t, "duplicate variable declaration '" + t.GetValue() + "'.");
    vars.push_back(var);
    if (_scanner->Peek().GetType() != tComma) break;
    Consume(tComma);
  }
  // parse type
  Consume(tColon);
  const CType *currType = type(isParam)->GetType();
  if (isParam && currType->IsArray())  // array in parameter; pass by reference
    currType = tm->GetPointer(currType);
  // fix symbol types
  for (auto var: vars) {
    var->SetDataType(currType);
    if (isParam) sp->AddParam(dynamic_cast<CSymParam*>(var));
  }

  return n;
}

void CParser::varDeclaration(CAstScope *s)
{
  //
  // varDeclaration ::=
  // [ "var" varDecl ";" { varDecl ";" } ].
  //
  // FIRST(varDecl)
  // = { tIdent }
  // FOLLOW(varDeclaration)
  // = { tBegin, tProcedure, tFunction}
  //
  int idx = 0;
  if (_scanner->Peek().GetType() == tVarDecl) {
    Consume(tVarDecl);
    do {
      idx += varDecl(s, false, idx);
      Consume(tSemicolon);
    } while (_scanner->Peek().GetType() == tIdent);
  }
}

void CParser::paramDeclaration(CAstScope *s)
{
  //
  // paramDeclaration ::=
  // "(" [ varDecl { ";" varDecl } ] ")".
  //
  // FIRST(varDecl) =
  // { tIdent }
  //
  Consume(tLParens);
  int idx = 0;
  if (_scanner->Peek().GetType() == tIdent) {
    while (true) {
      idx += varDecl(s, true, idx);
      if (_scanner->Peek().GetType() != tSemicolon) break;
      Consume(tSemicolon);
    }
  }
  Consume(tRParens);
}

CAstProcedure* CParser::subroutineDeclaration(CAstScope *s)
{
  // subroutineDeclaration
  // ::= (procDeclaration | funcDeclaration) subroutineBody ident ";".
  //
  // procDeclaration
  // ::= "procedure" ident [ paramDeclaration ] ";".
  // funcDeclaration
  // ::= "function" ident [ paramDeclaration ] ":" type ";".
  // subroutineBody
  // ::= varDeclaration "begin" statSequence "end".
  //
  // FIRST(procDeclaration)
  // = { tProcedure }
  // FIRST(funcDeclaration)
  // = { tFunction }
  // FIRST(paramDeclaration)
  // = { tLParens }
  //
  CToken t1, t2, t3;
  CAstStatement *statseq = NULL;
  const CType *tp;
  // determine procedure or function
  bool isFunction = _scanner->Peek().GetType() == tFunction;

  // declaration
  Consume(isFunction ? tFunction : tProcedure, &t1);
  Consume(tIdent, &t2);
  // currently we have no type info for function return,
  // so make symbol with temporary null type and fix later
  CSymProc *sym = new CSymProc(t2.GetValue(), CTypeManager::Get()->GetNull());
  if(!s->GetSymbolTable()->AddSymbol(sym))
    SetError(t2, "duplicate procedure/function declaration '"
             + t2.GetValue() + "'.");
  CAstProcedure *sp = new CAstProcedure(t1, t2.GetValue(), s, sym);

  // parameters
  if (_scanner->Peek().GetType() == tLParens)
    paramDeclaration(sp);

  // detect type of subroutine
  if (isFunction) {
    Consume(tColon);
    auto expr = type(false);
    tp = expr->GetType();
    if (!tp->IsBoolean() && !tp->IsInt() && !tp->IsChar())
      SetError(expr->GetToken(), "invalid composite type for function.");
    sym->SetDataType(tp);
  }
  Consume(tSemicolon);

  // variables
  varDeclaration(sp);

  // function body
  Consume(tBegin);
  statseq = statSequence(sp);
  Consume(tEnd);
  Consume(tIdent, &t3);
  Consume(tSemicolon);
  sp->SetStatementSequence(statseq);
  if (t2.GetValue() != t3.GetValue())
    SetError(t3, "procedure/function identifier mismatch ('" +
             t2.GetValue() + "' != '" + t3.GetValue() + "').");
  return sp;
}


CAstStatAssign* CParser::assignStatement(CAstScope *s, CToken ident)
{
  //
  // assignStatement ::=
  // qualident ":=" expression.
  //
  // due to the conflict w/ callStatement in parsing statement,
  // the first ident token is passed through function args
  //
  assert(ident.GetType() == tIdent);
  CToken t;
  CAstDesignator *lhs;
  CAstExpression *rhs;
  lhs = qualident(s, ident);
  Consume(tAssign, &t);
  rhs = expression(s);
  return new CAstStatAssign(t, lhs, rhs);
}

CAstStatCall* CParser::callStatement(CAstScope *s, CToken ident)
{
  //
  // callStatement ::=
  // subroutineCall.
  //
  // due to the conflict w/ assignStatement in parsing statement,
  // the first ident token is passed through function args
  //
  assert(ident.GetType() == tIdent);
  return new CAstStatCall(ident, subroutineCall(s, ident));
}

CAstStatIf* CParser::ifStatement(CAstScope *s)
{
  //
  // ifStatement ::=
  // "if" "(" expression ")" "then" statSequence [ "else" statSequence ] "end".
  //
  CAstExpression *cond;
  CAstStatement *ifBody, *elseBody = NULL;
  CToken t;
  Consume(tIf, &t);
  Consume(tLParens);
  cond = expression(s);
  Consume(tRParens);
  Consume(tThen);
  ifBody = statSequence(s);
  if (_scanner->Peek().GetType() == tElse) {
    Consume(tElse);
    elseBody = statSequence(s);
  }
  Consume(tEnd);
  return new CAstStatIf(t, cond, ifBody, elseBody);
}

CAstStatWhile* CParser::whileStatement(CAstScope *s)
{
  //
  // whileStatement ::=
  // "while" "(" expression ")" "do" statSequence "end".
  //
  CAstExpression *cond;
  CAstStatement *body;
  CToken t;
  Consume(tWhile, &t);
  Consume(tLParens);
  cond = expression(s);
  Consume(tRParens);
  Consume(tDo);
  body = statSequence(s);
  Consume(tEnd);
  return new CAstStatWhile(t, cond, body);
}

CAstStatReturn* CParser::returnStatement(CAstScope *s)
{
  //
  // returnStatement ::=
  // "return" [ expression ].
  //
  // FIRST(expression)
  // = { tPlusMinus, tIdent, tLParens, tExcl, tNumber, tBool, tChar, tString }
  // FOLLOW(returnStatement)
  // = { tEnd, tElse, tSemicolon}
  //
  CAstExpression *expr = NULL;
  CToken t;
  Consume(tReturn, &t);
  EToken tt = _scanner->Peek().GetType();
  if (tt != tEnd && tt != tElse && tt != tSemicolon) expr = expression(s);
  return new CAstStatReturn(t, s, expr);
}

CAstStatement* CParser::statement(CAstScope *s)
{
  //
  // statement ::=
  // assignStatement | callStatement | ifStatement | whileStatement
  // | returnStatement.
  //
  // FIRST(assignStatement) = FIRST(callStatement)    << conflict!
  // = { tIdent }
  // FIRST(ifStatement)
  // = { tIf }
  // FIRST(whileStatement)
  // = { tWhile }
  // FIRST(returnStatement)
  // = { tReturn }
  //
  CAstStatement *st;
  CToken t;
  EToken tt = _scanner->Peek().GetType();
  switch (tt) {
    case tIdent:
      // peek one more token to determine node type
      Consume(tIdent, &t);
      switch (_scanner->Peek().GetType()) {
        case tAssign:
        case tLBrak:
            st = assignStatement(s, t);
            break;
        case tLParens:
          st = callStatement(s, t);
          break;
        default:
          SetError(t, "statement expected.");
      }
      break;
    case tIf:
      st = ifStatement(s);
      break;
    case tWhile:
      st = whileStatement(s);
      break;
    case tReturn:
      st = returnStatement(s);
      break;
    default:
      SetError(_scanner->Peek(), "statement expected.");
  }
  return st;
}

CAstStatement* CParser::statSequence(CAstScope *s)
{
  //
  // statSequence ::=
  // [ statement { ";" statement } ].
  //
  // FIRST(statement)
  // = { tIdent, tIf, tWhile, tReturn }
  // FOLLOW(statSequence)
  // = { tElse, tEnd }
  //
  CAstStatement *head = NULL;
  CAstStatement *tail;
  EToken tt = _scanner->Peek().GetType();
  if (tt == tEnd || tt == tElse)
    return head;
  head = statement(s);
  tail = head;
  while (_scanner->Peek().GetType() == tSemicolon) {
    Consume(tSemicolon);
    tail->SetNext(statement(s));
    tail = tail->GetNext();
  }
  return head;
}


CAstDesignator* CParser::qualident(CAstScope *s, CToken ident)
{
  //
  // qualident ::=
  // ident { "[" expression "]" }.
  //
  // FOLLOW(qualident)
  // = { tAssign, tSemicolon, tComma, tRParens, tRBrak, tEnd, tElse }
  //   + factOp + termOp + relOp
  //
  // due to the conflict w/ subroutineCall in parsing factor,
  // the first ident token is passed through function args
  //
  assert(ident.GetType() == tIdent);
  CAstDesignator *qi = NULL;
  const CSymbol* symbol = s->GetSymbolTable()->FindSymbol(ident.GetValue());
  if (symbol == NULL)
    SetError(ident, "undefined identifier.");
  if (symbol->GetSymbolType() == stProcedure)
    SetError(ident, "designator expected.");

  if (_scanner->Peek().GetType() != tLBrak) {
    qi = new CAstDesignator(ident, symbol);
  } else {
    qi = new CAstArrayDesignator(ident, symbol);
    while (_scanner->Peek().GetType() == tLBrak) {
      Consume(tLBrak);
      dynamic_cast<CAstArrayDesignator*>(qi)->AddIndex(expression(s));
      Consume(tRBrak);
    }
    dynamic_cast<CAstArrayDesignator*>(qi)->IndicesComplete();
    auto t = qi->GetType();
    if (t == NULL)
      SetError(ident, "invalid array expression.");
    else if (t->IsArray())
      SetError(ident, "incomplete array expression (sub-arrays are not supported).");
  }
  return qi;
}

CAstFunctionCall* CParser::subroutineCall(CAstScope *s, CToken ident)
{
  //
  // subroutineCall ::=
  // ident "(" [ expression { "," expression } ] ")".
  //
  // FIRST(expression)
  // = { tPlusMinus, tIdent, tLParens, tExcl, tNumber, tBool, tChar, tString }
  //
  // due to the conflict w/ qualident in parsing factor,
  // the first ident token is passed through function args
  //
  assert(ident.GetType() == tIdent);
  CAstFunctionCall *fun = NULL;
  const CSymbol *symbol = s->GetSymbolTable()->FindSymbol(ident.GetValue());
  if (symbol == NULL)
    SetError(ident, "undefined identifier.");
  if (symbol->GetSymbolType() != stProcedure)
    SetError(ident, "invalid procedure/function identifier.");

  fun = new CAstFunctionCall(ident, dynamic_cast<const CSymProc*>(symbol));
  Consume(tLParens);
  if (_scanner->Peek().GetType() != tRParens)
    while (true) {
      CAstExpression *expr = expression(s);
      // check type of a parameter
      const CType *currType = expr->GetType();
      // if its type is array, then casting it into pointer
      if(currType != NULL && currType->IsArray())
        expr = new CAstSpecialOp(expr->GetToken(), opAddress, expr, NULL);
      fun->AddArg(expr);
      if (_scanner->Peek().GetType() == tRParens) break;
      Consume(tComma);
    }
  if (fun->GetNArgs() < fun->GetSymbol()->GetNParams())
    SetError(ident, "not enough arguments.");
  if (fun->GetNArgs() > fun->GetSymbol()->GetNParams())
    SetError(ident, "too many arguments.");
  Consume(tRParens);
  return fun;
}

CAstExpression* CParser::factor(CAstScope *s)
{
  //
  // factor ::=
  // qualident | subroutineCall | "(" expression ")" | "!" factor
  // | number | boolean | char | string.
  //
  // FIRST(qualident) = FIRST(subroutineCall)    << conflict!
  // = { tIdent }
  // FIRST(number)
  // = { tNumber }
  // FIRST(boolean)
  // = { tBool }
  // FIRST(char)
  // = { tChar }
  // FIRST(string)
  // = { tString }
  //
  CAstExpression *ftr = NULL;
  CToken t;
  switch (_scanner->Peek().GetType()) {
    case tIdent:
      // peek one more token to determine node type
      Consume(tIdent, &t);
      if (_scanner->Peek().GetType() == tLParens) ftr = subroutineCall(s, t);
      else ftr = qualident(s, t);
      break;
    case tLParens:
      Consume(tLParens);
      ftr = expression(s);
      Consume(tRParens);
      break;
    case tNot:
      Consume(tNot, &t);
      ftr = new CAstUnaryOp(t, opNot, factor(s));
      break;
    case tNumber:
      ftr = constNumber();
      break;
    case tBoolConst:
      ftr = constBoolean();
      break;
    case tCharConst:
      ftr = constChar();
      break;
    case tString:
      ftr = constString(s);
      break;
    default:
      SetError(_scanner->Peek(), "factor expected.");
      break;
  }

  return ftr;
}

CAstExpression* CParser::term(CAstScope *s)
{
  //
  // term ::=
  // factor { factOp factor }.
  //
  // factOp ::=
  // "*" | "/" | "&&".
  //
  // FOLLOW(term)
  // = { tSemicolon, tComma, tRParens, tRBrak, tEnd, tElse } + relOp + termOp
  //
  CAstExpression *trm;
  EOperation op;
  CToken t;
  trm = factor(s);
  EToken tt = _scanner->Peek().GetType();
  while (tt == tMulDiv | tt == tAnd) {
    Consume(tt, &t);
    op = tt == tMulDiv ? (t.GetValue()=="*" ? opMul : opDiv) : opAnd;
    trm = new CAstBinaryOp(t, op, trm, factor(s));
    tt = _scanner->Peek().GetType();
  }
  return trm;
}

CAstExpression* CParser::simpleexpr(CAstScope *s)
{
  //
  // simpleexpr ::=
  // ["+" | "-"] term { termOp term }.
  //
  // termOp ::=
  // "+" | "-" | "||".
  //
  // FOLLOW(simpleexpr)
  // = { tSemicolon, tComma, tRParens, tRBrak, tEnd, tElse } + relOp
  //
  CAstExpression *sexpr = NULL;
  EOperation op = opNop;
  CToken t;
  CAstConstant *tmp;

  // handling unary operator
  if (_scanner->Peek().GetType() == tPlusMinus) {
    Consume(tPlusMinus, &t);
    op = t.GetValue() == "+" ? opPos : opNeg;
  }
  sexpr = term(s);
  tmp = dynamic_cast<CAstConstant*>(sexpr);
  if (tmp != NULL && tmp->GetType()->IsInt()) {
    if (op == opNeg) {
      // parsed node is actually a single integer constant term;
      tmp->SetValue(-1*tmp->GetValue());
    }
  } else if (op != opNop) {
    sexpr = new CAstUnaryOp(t, op, sexpr);
  }

  EToken tt = _scanner->Peek().GetType();
  while (tt == tPlusMinus | tt == tOr) {
    Consume(tt, &t);
    op = tt == tPlusMinus ? (t.GetValue()=="+" ? opAdd : opSub) : opOr;
    sexpr = new CAstBinaryOp(t, op, sexpr, term(s));
    tt = _scanner->Peek().GetType();
  }
  return sexpr;
}

CAstExpression* CParser::expression(CAstScope* s)
{
  //
  // expression ::=
  // simpleexpr [ relOp simpleexpr ].
  //
  // relOp ::=
  // "=" | "#" | "<" | "<=" | ">" | ">=".
  //
  // FOLLOW(expression)
  // = { tSemicolon, tComma, tRParens, tRBrak, tEnd, tElse }
  //
  CAstExpression *expr;
  EOperation op;
  CToken t;
  expr = simpleexpr(s);
  if (_scanner->Peek().GetType() == tRelOp) {
    Consume(tRelOp, &t);
    if (t.GetValue() == "=")       op = opEqual;
    else if (t.GetValue() == "#")  op = opNotEqual;
    else if (t.GetValue() == "<")  op = opLessThan;
    else if (t.GetValue() == "<=") op = opLessEqual;
    else if (t.GetValue() == ">")  op = opBiggerThan;
    else if (t.GetValue() == ">=") op = opBiggerEqual;
    return new CAstBinaryOp(t, op, expr, simpleexpr(s));
  }
  return expr;
}


CAstConstant* CParser::constNumber(void)
{
  long long v;
  CToken t;
  Consume(tNumber, &t);
  errno = 0;
  v = strtoll(t.GetValue().c_str(), NULL, 10);
  if (errno != 0)
    SetError(t, "invalid number.");
  return new CAstConstant(t, CTypeManager::Get()->GetInt(), v);
}

CAstConstant* CParser::constBoolean(void)
{
  long long v;
  CToken t;
  Consume(tBoolConst, &t);
  v = t.GetValue() == "false" ? 0 : 1;  // 1: true, 0: false
  return new CAstConstant(t, CTypeManager::Get()->GetBool(), v);
}

CAstConstant* CParser::constChar(void)
{
  char c;
  string s;
  CToken t;
  Consume(tCharConst, &t);
  s = CToken::unescape(t.GetValue());
  if (s == "") c = 0;  // null character
  else c = s.at(0);
  return new CAstConstant(t, CTypeManager::Get()->GetChar(), c);
}

CAstStringConstant* CParser::constString(CAstScope *s)
{
  CToken t;
  Consume(tString, &t);
  return new CAstStringConstant(t, t.GetValue(), s);
}
