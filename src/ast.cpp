//------------------------------------------------------------------------------
/// @brief SnuPL abstract syntax tree
/// @author Bernhard Egger <bernhard@csap.snu.ac.kr>
/// @section changelog Change Log
/// 2012/09/14 Bernhard Egger created
/// 2013/05/22 Bernhard Egger reimplemented TAC generation
/// 2013/11/04 Bernhard Egger added typechecks for unary '+' operators
/// 2016/03/12 Bernhard Egger adapted to SnuPL/1
/// 2014/04/08 Bernhard Egger assignment 2: AST for SnuPL/-1
///
/// @section license_section License
/// Copyright (c) 2012-2016 Bernhard Egger
/// All rights reserved.
///
/// Redistribution and use in source and binary forms,  with or without modifi-
/// cation, are permitted provided that the following conditions are met:
///
/// - Redistributions of source code must retain the above copyright notice,
///   this list of conditions and the following disclaimer.
/// - Redistributions in binary form must reproduce the above copyright notice,
///   this list of conditions and the following disclaimer in the documentation
///   and/or other materials provided with the distribution.
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
/// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
/// IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
/// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER  OR CONTRIBUTORS BE
/// LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSE-
/// QUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF  SUBSTITUTE
/// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
/// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT
/// LIABILITY, OR TORT  (INCLUDING NEGLIGENCE OR OTHERWISE)  ARISING IN ANY WAY
/// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
/// DAMAGE.
//------------------------------------------------------------------------------

#include <iostream>
#include <cassert>
#include <cstring>

#include <typeinfo>

#include "ast.h"
using namespace std;


//------------------------------------------------------------------------------
// CAstNode
//
int CAstNode::_global_id = 0;

CAstNode::CAstNode(CToken token)
  : _token(token), _addr(NULL)
{
  _id = _global_id++;
}

CAstNode::~CAstNode(void)
{
  if (_addr != NULL) delete _addr;
}

int CAstNode::GetID(void) const
{
  return _id;
}

CToken CAstNode::GetToken(void) const
{
  return _token;
}

const CType* CAstNode::GetType(void) const
{
  return CTypeManager::Get()->GetNull();
}

string CAstNode::dotID(void) const
{
  ostringstream out;
  out << "node" << dec << _id;
  return out.str();
}

string CAstNode::dotAttr(void) const
{
  return " [label=\"" + dotID() + "\"]";
}

void CAstNode::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << dotID() << dotAttr() << ";" << endl;
}

CTacAddr* CAstNode::GetTacAddr(void) const
{
  return _addr;
}

ostream& operator<<(ostream &out, const CAstNode &t)
{
  return t.print(out);
}

ostream& operator<<(ostream &out, const CAstNode *t)
{
  return t->print(out);
}

//------------------------------------------------------------------------------
// CAstScope
//
CAstScope::CAstScope(CToken t, const string name, CAstScope *parent)
  : CAstNode(t), _name(name), _symtab(NULL), _parent(parent), _statseq(NULL),
    _cb(NULL)
{
  if (_parent != NULL) _parent->AddChild(this);
}

CAstScope::~CAstScope(void)
{
  delete _symtab;
  delete _statseq;
  delete _cb;
}

const string CAstScope::GetName(void) const
{
  return _name;
}

CAstScope* CAstScope::GetParent(void) const
{
  return _parent;
}

size_t CAstScope::GetNumChildren(void) const
{
  return _children.size();
}

CAstScope* CAstScope::GetChild(size_t i) const
{
  assert(i < _children.size());
  return _children[i];
}

CSymtab* CAstScope::GetSymbolTable(void) const
{
  assert(_symtab != NULL);
  return _symtab;
}

void CAstScope::SetStatementSequence(CAstStatement *statseq)
{
  _statseq = statseq;
}

CAstStatement* CAstScope::GetStatementSequence(void) const
{
  return _statseq;
}

bool CAstScope::TypeCheck(CToken *t, string *msg) const
{
  // catch all exceptions during typechecking
  try {
    // typecheck all statements
    for(CAstStatement *s = GetStatementSequence(); s != NULL; s = s->GetNext())
      if(!s->TypeCheck(t, msg)) return false;

    // recur to all subroutines
    for(auto child : _children)
      if(!child->TypeCheck(t, msg)) return false;
  } catch (...) {
    return false;
  }

  return true;
}

ostream& CAstScope::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << "CAstScope: '" << _name << "'" << endl;
  out << ind << "  symbol table:" << endl;
  _symtab->print(out, indent+4);
  out << ind << "  statement list:" << endl;
  CAstStatement *s = GetStatementSequence();
  if (s != NULL) {
    do {
      s->print(out, indent+4);
      s = s->GetNext();
    } while (s != NULL);
  } else {
    out << ind << "    empty." << endl;
  }

  out << ind << "  nested scopes:" << endl;
  if (_children.size() > 0) {
    for (size_t i=0; i<_children.size(); i++) {
      _children[i]->print(out, indent+4);
    }
  } else {
    out << ind << "    empty." << endl;
  }
  out << ind << endl;

  return out;
}

void CAstScope::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  CAstStatement *s = GetStatementSequence();
  if (s != NULL) {
    string prev = dotID();
    do {
      s->toDot(out, indent);
      out << ind << prev << " -> " << s->dotID() << " [style=dotted];" << endl;
      prev = s->dotID();
      s = s->GetNext();
    } while (s != NULL);
  }

  vector<CAstScope*>::const_iterator it = _children.begin();
  while (it != _children.end()) {
    CAstScope *s = *it++;
    s->toDot(out, indent);
    out << ind << dotID() << " -> " << s->dotID() << ";" << endl;
  }

}

CTacAddr* CAstScope::ToTac(CCodeBlock *cb)
{
  assert(cb != NULL);

  CAstStatement *s = GetStatementSequence();
  while (s != NULL) {
    CTacLabel *next = cb->CreateLabel();
    s->ToTac(cb, next);
    cb->AddInstr(next);
    s = s->GetNext();
  }
  cb->CleanupControlFlow();

  return NULL;
}

CCodeBlock* CAstScope::GetCodeBlock(void) const
{
  return _cb;
}

void CAstScope::SetSymbolTable(CSymtab *st)
{
  if (_symtab != NULL) delete _symtab;
  _symtab = st;
}

void CAstScope::AddChild(CAstScope *child)
{
  _children.push_back(child);
}


//------------------------------------------------------------------------------
// CAstModule
//
CAstModule::CAstModule(CToken t, const string name)
  : CAstScope(t, name, NULL)
{
  SetSymbolTable(new CSymtab());
}

CSymbol* CAstModule::CreateVar(const string ident, const CType *type)
{
  return new CSymGlobal(ident, type);
}

string CAstModule::dotAttr(void) const
{
  return " [label=\"m " + GetName() + "\",shape=box]";
}



//------------------------------------------------------------------------------
// CAstProcedure
//
CAstProcedure::CAstProcedure(CToken t, const string name,
                             CAstScope *parent, CSymProc *symbol)
  : CAstScope(t, name, parent), _symbol(symbol)
{
  assert(GetParent() != NULL);
  SetSymbolTable(new CSymtab(GetParent()->GetSymbolTable()));
  assert(_symbol != NULL);
}

CSymProc* CAstProcedure::GetSymbol(void) const
{
  return _symbol;
}

CSymbol* CAstProcedure::CreateVar(const string ident, const CType *type)
{
  return new CSymLocal(ident, type);
}

const CType* CAstProcedure::GetType(void) const
{
  return GetSymbol()->GetDataType();
}

string CAstProcedure::dotAttr(void) const
{
  return " [label=\"p/f " + GetName() + "\",shape=box]";
}


//------------------------------------------------------------------------------
// CAstType
//
CAstType::CAstType(CToken t, const CType *type)
  : CAstNode(t), _type(type)
{
  assert(type != NULL);
}

const CType* CAstType::GetType(void) const
{
  return _type;
}

ostream& CAstType::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << "CAstType (" << _type << ")" << endl;
  return out;
}


//------------------------------------------------------------------------------
// CAstStatement
//
CAstStatement::CAstStatement(CToken token)
  : CAstNode(token), _next(NULL)
{
}

CAstStatement::~CAstStatement(void)
{
  delete _next;
}

void CAstStatement::SetNext(CAstStatement *next)
{
  _next = next;
}

CAstStatement* CAstStatement::GetNext(void) const
{
  return _next;
}

CTacAddr* CAstStatement::ToTac(CCodeBlock *cb, CTacLabel *next)
{
  return NULL;
}


//------------------------------------------------------------------------------
// CAstStatAssign
//
CAstStatAssign::CAstStatAssign(CToken t,
                               CAstDesignator *lhs, CAstExpression *rhs)
  : CAstStatement(t), _lhs(lhs), _rhs(rhs)
{
  assert(lhs != NULL);
  assert(rhs != NULL);
}

CAstDesignator* CAstStatAssign::GetLHS(void) const
{
  return _lhs;
}

CAstExpression* CAstStatAssign::GetRHS(void) const
{
  return _rhs;
}

bool CAstStatAssign::TypeCheck(CToken *t, string *msg) const
{
  // by assertion, lhs and rhs are not NULL
  CAstDesignator *lhs = GetLHS();
  CAstExpression *rhs = GetRHS();

  // typecheck lhs
  // lhs should not be an array
  if(!lhs->TypeCheck(t, msg)) return false;
  if(lhs->GetType()->IsArray()) {
    *t = GetToken();
    stringstream ss;
    ss << "assignments to compound types are not supported." << endl
       << "  LHS: " << lhs->GetType() << endl
       << "  RHS: " << rhs->GetType();
    *msg = ss.str();
    return false;
  }

  // typecheck rhs
  // rhs should match lhs
  if(!rhs->TypeCheck(t, msg)) return false;
  if(!lhs->GetType()->Match(rhs->GetType())) {
    *t = GetToken();
    stringstream ss;
    ss << "incompatible types in assignment:" << endl
       << "  LHS: " << lhs->GetType() << endl
       << "  RHS: " << rhs->GetType();
    *msg = ss.str();
    return false;
  }

  return true;
}

const CType* CAstStatAssign::GetType(void) const
{
  return _lhs->GetType();
}

ostream& CAstStatAssign::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << ":=" << " ";

  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";

  out << endl;

  _lhs->print(out, indent+2);
  _rhs->print(out, indent+2);

  return out;
}

string CAstStatAssign::dotAttr(void) const
{
  return " [label=\":=\",shape=box]";
}

void CAstStatAssign::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  _lhs->toDot(out, indent);
  out << ind << dotID() << "->" << _lhs->dotID() << ";" << endl;
  _rhs->toDot(out, indent);
  out << ind << dotID() << "->" << _rhs->dotID() << ";" << endl;
}

CTacAddr* CAstStatAssign::ToTac(CCodeBlock *cb, CTacLabel *next)
{
  assert(cb != NULL);

  CTacAddr* src = GetRHS()->ToTac(cb);
  CTacAddr* dst = GetLHS()->ToTac(cb);
  cb->AddInstr(new CTacInstr(opAssign, dst, src));

  return NULL;
}


//------------------------------------------------------------------------------
// CAstStatCall
//
CAstStatCall::CAstStatCall(CToken t, CAstFunctionCall *call)
  : CAstStatement(t), _call(call)
{
  assert(call != NULL);
}

CAstFunctionCall* CAstStatCall::GetCall(void) const
{
  return _call;
}

bool CAstStatCall::TypeCheck(CToken *t, string *msg) const
{
  return GetCall()->TypeCheck(t, msg);
}

ostream& CAstStatCall::print(ostream &out, int indent) const
{
  _call->print(out, indent);

  return out;
}

string CAstStatCall::dotID(void) const
{
  return _call->dotID();
}

string CAstStatCall::dotAttr(void) const
{
  return _call->dotAttr();
}

void CAstStatCall::toDot(ostream &out, int indent) const
{
  _call->toDot(out, indent);
}

CTacAddr* CAstStatCall::ToTac(CCodeBlock *cb, CTacLabel *next)
{
  assert(cb != NULL);

  GetCall()->ToTac(cb);

  return NULL;
}


//------------------------------------------------------------------------------
// CAstStatReturn
//
CAstStatReturn::CAstStatReturn(CToken t, CAstScope *scope, CAstExpression *expr)
  : CAstStatement(t), _scope(scope), _expr(expr)
{
  assert(scope != NULL);
}

CAstScope* CAstStatReturn::GetScope(void) const
{
  return _scope;
}

CAstExpression* CAstStatReturn::GetExpression(void) const
{
  return _expr;
}

bool CAstStatReturn::TypeCheck(CToken *t, string *msg) const
{
  // by assertion, s is not NULL
  const CAstScope *s = GetScope();
  CAstExpression *e = GetExpression();

  bool isScopeNull = s->GetType()->IsNull();
  bool isExprNull = e == NULL;

  // if scope is NULL but expr is not
  if (isScopeNull && !isExprNull) {
    *t = e->GetToken();
    *msg = "superfluous expression after return.";
    return false;
  }

  // if expr is NULL but scope is not
  if (!isScopeNull && isExprNull) {
    *t = GetToken();
    *msg = "expression expected after return.";
    return false;
  }

  // if scope is NULL and expr is NULL, no problem
  // if scope is not NULL and expr is not NULL
  if (!isScopeNull && !isExprNull) {

    // expr should match scope
    if (!e->TypeCheck(t, msg)) return false;
    if (!s->GetType()->Match(e->GetType())) {
      *t = e->GetToken();
      *msg = "return type mismatch.";
      return false;
    }
  }

  return true;
}

const CType* CAstStatReturn::GetType(void) const
{
  const CType *t = NULL;

  if (GetExpression() != NULL) {
    t = GetExpression()->GetType();
  } else {
    t = CTypeManager::Get()->GetNull();
  }

  return t;
}

ostream& CAstStatReturn::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << "return" << " ";

  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";

  out << endl;

  if (_expr != NULL) _expr->print(out, indent+2);

  return out;
}

string CAstStatReturn::dotAttr(void) const
{
  return " [label=\"return\",shape=box]";
}

void CAstStatReturn::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  if (_expr != NULL) {
    _expr->toDot(out, indent);
    out << ind << dotID() << "->" << _expr->dotID() << ";" << endl;
  }
}

CTacAddr* CAstStatReturn::ToTac(CCodeBlock *cb, CTacLabel *next)
{
  assert(cb != NULL);

  auto expr = GetExpression();
  cb->AddInstr(new CTacInstr(opReturn, NULL,
                             expr != NULL ? expr->ToTac(cb) : NULL));

  return NULL;
}


//------------------------------------------------------------------------------
// CAstStatIf
//
CAstStatIf::CAstStatIf(CToken t, CAstExpression *cond,
                       CAstStatement *ifBody, CAstStatement *elseBody)
  : CAstStatement(t), _cond(cond), _ifBody(ifBody), _elseBody(elseBody)
{
  assert(cond != NULL);
}

CAstExpression* CAstStatIf::GetCondition(void) const
{
  return _cond;
}

CAstStatement* CAstStatIf::GetIfBody(void) const
{
  return _ifBody;
}

CAstStatement* CAstStatIf::GetElseBody(void) const
{
  return _elseBody;
}

bool CAstStatIf::TypeCheck(CToken *t, string *msg) const
{
  // by assertion, cond is not NULL
  CAstExpression *cond = GetCondition();

  // cond should be boolean
  if(!cond->TypeCheck(t, msg)) return false;
  if(!cond->GetType()->IsBoolean()) {
    *t = cond->GetToken();
    *msg = "boolean expression expected.";
    return false;
  }

  // recur to statements of the bodies
  for(CAstStatement *s = GetIfBody(); s != NULL; s = s->GetNext())
    if(!s->TypeCheck(t, msg)) return false;
  for(CAstStatement *s = GetElseBody(); s != NULL; s = s->GetNext())
    if(!s->TypeCheck(t, msg)) return false;

  return true;
}

ostream& CAstStatIf::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << "if cond" << endl;
  _cond->print(out, indent+2);
  out << ind << "if-body" << endl;
  if (_ifBody != NULL) {
    CAstStatement *s = _ifBody;
    do {
      s->print(out, indent+2);
      s = s->GetNext();
    } while (s != NULL);
  } else out << ind << "  empty." << endl;
  out << ind << "else-body" << endl;
  if (_elseBody != NULL) {
    CAstStatement *s = _elseBody;
    do {
      s->print(out, indent+2);
      s = s->GetNext();
    } while (s != NULL);
  } else out << ind << "  empty." << endl;

  return out;
}

string CAstStatIf::dotAttr(void) const
{
  return " [label=\"if\",shape=box]";
}

void CAstStatIf::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  _cond->toDot(out, indent);
  out << ind << dotID() << "->" << _cond->dotID() << ";" << endl;

  if (_ifBody != NULL) {
    CAstStatement *s = _ifBody;
    if (s != NULL) {
      string prev = dotID();
      do {
        s->toDot(out, indent);
        out << ind << prev << " -> " << s->dotID() << " [style=dotted];"
            << endl;
        prev = s->dotID();
        s = s->GetNext();
      } while (s != NULL);
    }
  }

  if (_elseBody != NULL) {
    CAstStatement *s = _elseBody;
    if (s != NULL) {
      string prev = dotID();
      do {
        s->toDot(out, indent);
        out << ind << prev << " -> " << s->dotID() << " [style=dotted];" 
            << endl;
        prev = s->dotID();
        s = s->GetNext();
      } while (s != NULL);
    }
  }
}

CTacAddr* CAstStatIf::ToTac(CCodeBlock *cb, CTacLabel *next)
{
  assert(cb != NULL && next != NULL);

  CTacLabel* ltrue = cb->CreateLabel("if_true");
  CTacLabel* lfalse = cb->CreateLabel("if_false");
  CTacLabel* ltmp;

  // create conditional jump
  GetCondition()->ToTac(cb, ltrue, lfalse);

  // if condition is true, evaluate this block
  cb->AddInstr(ltrue);
  CAstStatement* ifBody = GetIfBody();
  while (ifBody != NULL) {
    ltmp = cb->CreateLabel();
    ifBody->ToTac(cb, ltmp);
    cb->AddInstr(ltmp);
    ifBody = ifBody->GetNext();
  }
  // end of ifblock
  cb->AddInstr(new CTacInstr(opGoto, next));

  // if condition is false, evaluate this block
  cb->AddInstr(lfalse);
  CAstStatement* elseBody = GetElseBody();
  while (elseBody != NULL) {
    ltmp = cb->CreateLabel();
    elseBody->ToTac(cb, ltmp);
    cb->AddInstr(ltmp);
    elseBody = elseBody->GetNext();
  }
  // end of elseblock
  cb->AddInstr(new CTacInstr(opGoto, next));
  return NULL;
}


//------------------------------------------------------------------------------
// CAstStatWhile
//
CAstStatWhile::CAstStatWhile(CToken t,
                             CAstExpression *cond, CAstStatement *body)
  : CAstStatement(t), _cond(cond), _body(body)
{
  assert(cond != NULL);
}

CAstExpression* CAstStatWhile::GetCondition(void) const
{
  return _cond;
}

CAstStatement* CAstStatWhile::GetBody(void) const
{
  return _body;
}

bool CAstStatWhile::TypeCheck(CToken *t, string *msg) const
{
  // by assertion, cond is not NULL
  CAstExpression *cond = GetCondition();

  // cond should be boolean
  if(!cond->TypeCheck(t, msg)) return false;
  if(!cond->GetType()->IsBoolean()) {
    *t = cond->GetToken();
    *msg = "boolean expected as condition";
    return false;
  }

  // recur to statements of the body
  for(CAstStatement *s = GetBody(); s != NULL; s = s->GetNext())
    if(!s->TypeCheck(t, msg)) return false;

  return true;
}

ostream& CAstStatWhile::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << "while cond" << endl;
  _cond->print(out, indent+2);
  out << ind << "while-body" << endl;
  if (_body != NULL) {
    CAstStatement *s = _body;
    do {
      s->print(out, indent+2);
      s = s->GetNext();
    } while (s != NULL);
  }
  else out << ind << "  empty." << endl;

  return out;
}

string CAstStatWhile::dotAttr(void) const
{
  return " [label=\"while\",shape=box]";
}

void CAstStatWhile::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  _cond->toDot(out, indent);
  out << ind << dotID() << "->" << _cond->dotID() << ";" << endl;

  if (_body != NULL) {
    CAstStatement *s = _body;
    if (s != NULL) {
      string prev = dotID();
      do {
        s->toDot(out, indent);
        out << ind << prev << " -> " << s->dotID() << " [style=dotted];"
            << endl;
        prev = s->dotID();
        s = s->GetNext();
      } while (s != NULL);
    }
  }
}

CTacAddr* CAstStatWhile::ToTac(CCodeBlock *cb, CTacLabel *next)
{
  assert(cb != NULL && next != NULL);

  CTacLabel* lcond = cb->CreateLabel("while_cond");
  CTacLabel* lbody = cb->CreateLabel("while_body");
  cb->AddInstr(lcond);
  // create conditional jump
  GetCondition()->ToTac(cb, lbody, next);
  // if condition is true, evalutate below block; else skip
  cb->AddInstr(lbody);
  CAstStatement* body = GetBody();
  while (body != NULL) {
    CTacLabel* ltmp = cb->CreateLabel();
    body->ToTac(cb, ltmp);
    cb->AddInstr(ltmp);
    body = body->GetNext();
  }
  cb->AddInstr(new CTacInstr(opGoto, lcond));  // go back
  // end of block

  return NULL;
}


//------------------------------------------------------------------------------
// CAstExpression
//
CAstExpression::CAstExpression(CToken t)
  : CAstNode(t)
{
}

CTacAddr* CAstExpression::ToTac(CCodeBlock *cb)
{
  return NULL;
}

CTacAddr* CAstExpression::ToTac(CCodeBlock *cb,
                                CTacLabel *ltrue, CTacLabel *lfalse)
{
  return NULL;
}


//------------------------------------------------------------------------------
// CAstOperation
//
CAstOperation::CAstOperation(CToken t, EOperation oper)
  : CAstExpression(t), _oper(oper)
{
}

EOperation CAstOperation::GetOperation(void) const
{
  return _oper;
}


//------------------------------------------------------------------------------
// CAstBinaryOp
//
CAstBinaryOp::CAstBinaryOp(CToken t, EOperation oper,
                           CAstExpression *l,CAstExpression *r)
  : CAstOperation(t, oper), _left(l), _right(r)
{
  // these are the only binary operation we support for now
  assert((oper == opAdd)        || (oper == opSub)         ||
         (oper == opMul)        || (oper == opDiv)         ||
         (oper == opAnd)        || (oper == opOr)          ||
         (oper == opEqual)      || (oper == opNotEqual)    ||
         (oper == opLessThan)   || (oper == opLessEqual)   ||
         (oper == opBiggerThan) || (oper == opBiggerEqual)
        );
  assert(l != NULL);
  assert(r != NULL);
}

CAstExpression* CAstBinaryOp::GetLeft(void) const
{
  return _left;
}

CAstExpression* CAstBinaryOp::GetRight(void) const
{
  return _right;
}

bool CAstBinaryOp::TypeCheck(CToken *t, string *msg) const
{
  // by assertion, left and right is not NULL
  CAstExpression *left = GetLeft();
  CAstExpression *right = GetRight();
  CTypeManager *tm = CTypeManager::Get();

  if(!left->TypeCheck(t, msg)) return false;
  if (!right->TypeCheck(t, msg)) return false;

  // by assertion, operation is one of those
  bool result = false;
  switch (GetOperation()) {
    case opAdd:
    case opSub:
    case opMul:
    case opDiv:
      // if +, -, *, /, left should be integer
      result = left->GetType()->IsInt();
      break;

    case opEqual:
    case opNotEqual:
      // if =, #, left should be constant
      result = left->GetType()->IsBoolean() || left->GetType()->IsInt()
        || left->GetType()->IsChar();
      break;

    case opAnd:
    case opOr:
      // if &&, ||, left should be boolean
      result = left->GetType()->IsBoolean();
      break;

    case opLessThan:
    case opLessEqual:
    case opBiggerThan:
    case opBiggerEqual:
      // if >, >=, <, <=, left should be integer or character
      result = left->GetType()->IsInt() || left->GetType()->IsChar();
      break;
  }
  // in any case, right should match left
  result = result && left->GetType()->Match(right->GetType());
  if (!result) {
    *t = GetToken();
    stringstream ss;
    ss << GetOperation() << ": type mismatch." << endl
       << "  left  operand: " << left->GetType() << endl
       << "  right operand: " << right->GetType();
    *msg = ss.str();
  }
  return result;
}

const CType* CAstBinaryOp::GetType(void) const
{
  CTypeManager *tm =  CTypeManager::Get();

  // by assertion, operation is one of those
  switch (GetOperation()) {
    case opAdd:
    case opSub:
    case opMul:
    case opDiv:
      return tm->GetInt();
    case opEqual:
    case opNotEqual:
    case opAnd:
    case opOr:
    case opLessThan:
    case opLessEqual:
    case opBiggerThan:
    case opBiggerEqual:
      return tm->GetBool();
  }
}

ostream& CAstBinaryOp::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << GetOperation() << " ";

  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";

  out << endl;

  _left->print(out, indent+2);
  _right->print(out, indent+2);

  return out;
}

string CAstBinaryOp::dotAttr(void) const
{
  ostringstream out;
  out << " [label=\"" << GetOperation() << "\",shape=box]";
  return out.str();
}

void CAstBinaryOp::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  _left->toDot(out, indent);
  out << ind << dotID() << "->" << _left->dotID() << ";" << endl;
  _right->toDot(out, indent);
  out << ind << dotID() << "->" << _right->dotID() << ";" << endl;
}

CTacAddr* CAstBinaryOp::ToTac(CCodeBlock *cb)
{
  assert(cb != NULL);

  CTacTemp* result;
  EOperation op = GetOperation();
  if (op == opAdd || op == opSub || op == opMul || op == opDiv) {
    // integer expression; just evaluate it
    CTacAddr* loper = GetLeft()->ToTac(cb);
    CTacAddr* roper = GetRight()->ToTac(cb);
    result = cb->CreateTemp(GetType());
    cb->AddInstr(new CTacInstr(op, result, loper, roper));
  } else {
    // boolean expression;
    CTacLabel *ltrue = cb->CreateLabel();
    CTacLabel *lfalse = cb->CreateLabel();
    CTacLabel *lfinal = cb->CreateLabel();
    // create conditional jump
    ToTac(cb, ltrue, lfalse);
    result = cb->CreateTemp(GetType());
    cb->AddInstr(ltrue);
    // if condition is true, the result of operation is true, i.e., 1
    cb->AddInstr(new CTacInstr(opAssign, result, new CTacConst(1)));
    cb->AddInstr(new CTacInstr(opGoto, lfinal));
    cb->AddInstr(lfalse);
    // if condition is false, the result of operation is false, i.e., 0
    cb->AddInstr(new CTacInstr(opAssign, result, new CTacConst(0)));
    cb->AddInstr(lfinal);
  }

  return result;
}

CTacAddr* CAstBinaryOp::ToTac(CCodeBlock *cb,
                              CTacLabel *ltrue, CTacLabel *lfalse)
{
  assert(cb != NULL && ltrue != NULL && lfalse != NULL);
  assert(GetType()->IsBoolean());

  CTacLabel *ltest = cb->CreateLabel();
  EOperation op = GetOperation();
  if (op == opAnd || op == opOr) {
    // using short circuit
    GetLeft()->ToTac(cb, op == opOr ? ltrue : ltest,
                     op == opAnd ? lfalse : ltest);
    cb->AddInstr(ltest);
    GetRight()->ToTac(cb, ltrue, lfalse);
  } else {
    CTacAddr* loper = GetLeft()->ToTac(cb);
    CTacAddr* roper = GetRight()->ToTac(cb);
    cb->AddInstr(new CTacInstr(op, ltrue, loper, roper));
    cb->AddInstr(new CTacInstr(opGoto, lfalse));
  }

  return NULL;
}


//------------------------------------------------------------------------------
// CAstUnaryOp
//
CAstUnaryOp::CAstUnaryOp(CToken t, EOperation oper, CAstExpression *e)
  : CAstOperation(t, oper), _operand(e)
{
  assert((oper == opNeg) || (oper == opPos) || (oper == opNot));
  assert(e != NULL);
}

CAstExpression* CAstUnaryOp::GetOperand(void) const
{
  return _operand;
}

bool CAstUnaryOp::TypeCheck(CToken *t, string *msg) const
{
  // by assertion, oper is not NULL
  CAstExpression *oper = GetOperand();
  CTypeManager *tm = CTypeManager::Get();

  if(!oper->TypeCheck(t, msg)) return false;

  // by assertion, operation is one of those
  bool result = false;
  switch (GetOperation()) {
    case opNeg:
    case opPos:
      // if -, +, oper should be integer
      result = oper->GetType()->IsInt();
      break;

    case opNot:
      // if !, oper should be boolean
      result = oper->GetType()->IsBoolean();
      break;
  }
  if (!result) {
    *t = GetToken();
    stringstream ss;
    ss << GetOperation() << ": type mismatch." << endl
       << "  operand:       " << oper->GetType() << endl;
    *msg = ss.str();
  }
  return result;
}

const CType* CAstUnaryOp::GetType(void) const
{
  CTypeManager *tm = CTypeManager::Get();
  // by assertion, operation is one of those
  switch (GetOperation()) {
    case opNeg:
    case opPos:
      return tm->GetInt();
    case opNot:
      return tm->GetBool();
  }
}

ostream& CAstUnaryOp::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << GetOperation() << " ";

  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";
  out << endl;

  _operand->print(out, indent+2);

  return out;
}

string CAstUnaryOp::dotAttr(void) const
{
  ostringstream out;
  out << " [label=\"" << GetOperation() << "\",shape=box]";
  return out.str();
}

void CAstUnaryOp::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  _operand->toDot(out, indent);
  out << ind << dotID() << "->" << _operand->dotID() << ";" << endl;
}

CTacAddr* CAstUnaryOp::ToTac(CCodeBlock *cb)
{
  assert(cb != NULL);

  CTacTemp* result;
  EOperation op = GetOperation();
  if (op == opNot) {
    // boolean expression;
    CTacLabel *ltrue = cb->CreateLabel();
    CTacLabel *lfalse = cb->CreateLabel();
    CTacLabel *lfinal = cb->CreateLabel();
    ToTac(cb, ltrue, lfalse);
    // create conditional jump
    result = cb->CreateTemp(GetType());
    cb->AddInstr(ltrue);
    // if condition is true, the result of operation is true, i.e., 1
    cb->AddInstr(new CTacInstr(opAssign, result, new CTacConst(1)));
    cb->AddInstr(new CTacInstr(opGoto, lfinal));
    cb->AddInstr(lfalse);
    // if condition is false, the result of operation is false, i.e., 0
    cb->AddInstr(new CTacInstr(opAssign, result, new CTacConst(0)));
    cb->AddInstr(lfinal);
  } else {
    // integer expression;
    CTacAddr* operand = GetOperand()->ToTac(cb);
    result = cb->CreateTemp(GetType());
    cb->AddInstr(new CTacInstr(op, result, operand));
  }

  return result;
}

CTacAddr* CAstUnaryOp::ToTac(CCodeBlock *cb,
                             CTacLabel *ltrue, CTacLabel *lfalse)
{
  assert(cb != NULL && ltrue != NULL && lfalse != NULL);
  assert(GetType()->IsBoolean());

  // only opNot is possible; just changing T/F is enough
  GetOperand()->ToTac(cb, lfalse, ltrue);

  return NULL;
}


//------------------------------------------------------------------------------
// CAstSpecialOp
//
CAstSpecialOp::CAstSpecialOp(CToken t, EOperation oper, CAstExpression *e,
                             const CType *type)
  : CAstOperation(t, oper), _operand(e), _type(type)
{
  assert((oper == opAddress) || (oper == opDeref) || (oper = opCast));
  assert(e != NULL);
  assert(((oper != opCast) && (type == NULL)) ||
         ((oper == opCast) && (type != NULL)));
}

CAstExpression* CAstSpecialOp::GetOperand(void) const
{
  return _operand;
}

bool CAstSpecialOp::TypeCheck(CToken *t, string *msg) const
{
  // by assertion, oper is not NULL
  CAstExpression *oper = GetOperand();

  if (!oper->TypeCheck(t, msg)) return false;

  // by assertion, operation is one of those
  switch (GetOperation()) {
    case opAddress:
      // any operand can have pointer
      break;

    case opDeref:
      // derefer pointers only
      if (!oper->GetType()->IsPointer()) {
        *t = oper->GetToken();
        *msg = "dereference of non-pointer";
        return false;
      }
      break;

    case opCast:
      // typechecks on castings are handled on respective cases
      // currently no castings are generated in AST
      break;
  }

  return true;
}

const CType* CAstSpecialOp::GetType(void) const
{
  // by assertion, oper is not NULL
  const CAstExpression *oper = GetOperand();
  CTypeManager *tm = CTypeManager::Get();

  // by assertion, operation is one of those
  switch (GetOperation()) {
    case opAddress:
      // pointer of oper
      return tm->GetPointer(oper->GetType());

    case opDeref:
      // if oper is pointer, oper's base
      // else NULL
      if (oper->GetType()->IsPointer())
        return dynamic_cast<const CPointerType*>(oper->GetType())->GetBaseType();
      else
        return NULL;

    case opCast:
      // type of destination of casting
      return _type;
  }
}

ostream& CAstSpecialOp::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << GetOperation() << " ";

  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";
  out << endl;

  _operand->print(out, indent+2);

  return out;
}

string CAstSpecialOp::dotAttr(void) const
{
  ostringstream out;
  out << " [label=\"" << GetOperation() << "\",shape=box]";
  return out.str();
}

void CAstSpecialOp::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  _operand->toDot(out, indent);
  out << ind << dotID() << "->" << _operand->dotID() << ";" << endl;
}

CTacAddr* CAstSpecialOp::ToTac(CCodeBlock *cb)
{
  assert(cb != NULL);

  CTacAddr* operand = GetOperand()->ToTac(cb);
  CTacTemp* result = cb->CreateTemp(GetType());
  cb->AddInstr(new CTacInstr(GetOperation(), result, operand));

  return result;
}


//------------------------------------------------------------------------------
// CAstFunctionCall
//
CAstFunctionCall::CAstFunctionCall(CToken t, const CSymProc *symbol)
  : CAstExpression(t), _symbol(symbol)
{
  assert(symbol != NULL);
}

const CSymProc* CAstFunctionCall::GetSymbol(void) const
{
  return _symbol;
}

void CAstFunctionCall::AddArg(CAstExpression *arg)
{
  _arg.push_back(arg);
}

int CAstFunctionCall::GetNArgs(void) const
{
  return (int)_arg.size();
}

CAstExpression* CAstFunctionCall::GetArg(int index) const
{
  assert((index >= 0) && (index < _arg.size()));
  return _arg[index];
}

bool CAstFunctionCall::TypeCheck(CToken *t, string *msg) const
{
  // number of arguments is checked in parser
  assert (GetSymbol()->GetNParams() == GetNArgs());
  CAstExpression *arg;

  for(int i = 0; i < GetNArgs(); i++) {
    arg = GetArg(i);

    // arg should match parameter
    if (!arg->TypeCheck(t, msg)) return false;
    const CType *expect = GetSymbol()->GetParam(i)->GetDataType();
    if (!expect->Match(arg->GetType())) {
      *t = arg->GetToken();
      stringstream ss;
      ss << "parameter " << i+1 << ": argument type mismatch." << endl
         << "  expected " << expect << endl
         << "  got      " << arg->GetType();
      *msg = ss.str();
      return false;
    }
  }
  return true;
}

const CType* CAstFunctionCall::GetType(void) const
{
  return GetSymbol()->GetDataType();
}

ostream& CAstFunctionCall::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << "call " << _symbol << " ";
  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";
  out << endl;

  for (size_t i=0; i<_arg.size(); i++) {
    _arg[i]->print(out, indent+2);
  }

  return out;
}

string CAstFunctionCall::dotAttr(void) const
{
  ostringstream out;
  out << " [label=\"call " << _symbol->GetName() << "\",shape=box]";
  return out.str();
}

void CAstFunctionCall::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  for (size_t i=0; i<_arg.size(); i++) {
    _arg[i]->toDot(out, indent);
    out << ind << dotID() << "->" << _arg[i]->dotID() << ";" << endl;
  }
}

CTacAddr* CAstFunctionCall::ToTac(CCodeBlock *cb)
{
  assert(cb != NULL);

  int n = GetNArgs();
  // should pass arguments in reverse order
  for (int i = n-1; i >= 0; --i)
    cb->AddInstr(new CTacInstr(opParam, new CTacConst(i),
                               GetArg(i)->ToTac(cb)));
  CTacTemp* result = NULL;
  if (!GetType()->IsNull()) result = cb->CreateTemp(GetType());
  cb->AddInstr(new CTacInstr(opCall, result, new CTacName(GetSymbol())));

  return result;
}

CTacAddr* CAstFunctionCall::ToTac(CCodeBlock *cb,
                                  CTacLabel *ltrue, CTacLabel *lfalse)
{
  assert(cb != NULL && ltrue != NULL && lfalse != NULL);
  assert(GetType()->IsBoolean());

  CTacAddr* tmp = ToTac(cb);
  cb->AddInstr(new CTacInstr(opEqual, ltrue, tmp, new CTacConst(1)));
  cb->AddInstr(new CTacInstr(opGoto, lfalse));

  return NULL;
}



//------------------------------------------------------------------------------
// CAstOperand
//
CAstOperand::CAstOperand(CToken t)
  : CAstExpression(t)
{
}


//------------------------------------------------------------------------------
// CAstDesignator
//
CAstDesignator::CAstDesignator(CToken t, const CSymbol *symbol)
  : CAstOperand(t), _symbol(symbol)
{
  assert(symbol != NULL);
}

const CSymbol* CAstDesignator::GetSymbol(void) const
{
  return _symbol;
}

bool CAstDesignator::TypeCheck(CToken *t, string *msg) const
{
  return true;
}

const CType* CAstDesignator::GetType(void) const
{
  return GetSymbol()->GetDataType();
}

ostream& CAstDesignator::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << _symbol << " ";

  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";

  out << endl;

  return out;
}

string CAstDesignator::dotAttr(void) const
{
  ostringstream out;
  out << " [label=\"" << _symbol->GetName();
  out << "\",shape=ellipse]";
  return out.str();
}

void CAstDesignator::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);
}

CTacAddr* CAstDesignator::ToTac(CCodeBlock *cb)
{
  assert(cb != NULL);

  return new CTacName(GetSymbol());
}

CTacAddr* CAstDesignator::ToTac(CCodeBlock *cb,
                                CTacLabel *ltrue, CTacLabel *lfalse)
{
  assert(cb != NULL && ltrue != NULL && lfalse != NULL);
  assert(GetType()->IsBoolean());

  // use equality to test T/F
  cb->AddInstr(new CTacInstr(opEqual, ltrue, new CTacName(GetSymbol()),
                             new CTacConst(1)));
  cb->AddInstr(new CTacInstr(opGoto, lfalse));

  return NULL;
}


//------------------------------------------------------------------------------
// CAstArrayDesignator
//
CAstArrayDesignator::CAstArrayDesignator(CToken t, const CSymbol *symbol)
  : CAstDesignator(t, symbol), _done(false), _offset(NULL)
{
}

void CAstArrayDesignator::AddIndex(CAstExpression *idx)
{
  assert(!_done);
  _idx.push_back(idx);
}

void CAstArrayDesignator::IndicesComplete(void)
{
  assert(!_done);
  _done = true;
}

int CAstArrayDesignator::GetNIndices(void) const
{
  return (int)_idx.size();
}

CAstExpression* CAstArrayDesignator::GetIndex(int index) const
{
  assert((index >= 0) && (index < _idx.size()));
  return _idx[index];
}

bool CAstArrayDesignator::TypeCheck(CToken *t, string *msg) const
{
  const CType *result = GetSymbol()->GetDataType();
  // derefer if it is pointer
  if (result->IsPointer())
    result = dynamic_cast<const CPointerType*>(result)->GetBaseType();

  // result type should be array
  if (!result->IsArray()) {
    *t = GetToken();
    *msg = "invalid array expression.";
    return false;
  }

  // number of indices <= number of dimensions
  if (GetNIndices() > dynamic_cast<const CArrayType*>(result)->GetNDim()) {
    *t = GetToken();
    *msg = "invalid array expression.";
    return false;
  }

  CAstExpression *e;
  for (int i = 0; i < GetNIndices(); ++i){
    e = GetIndex(i);
    // index should be integer
    if (!e->TypeCheck(t, msg)) return false;
    if (!e->GetType()->IsInt()) {
      *t = e->GetToken();
      *msg = "invalid array index expression.";
      return false;
    }
    result = dynamic_cast<const CArrayType*>(result)->GetInnerType();
  }

  assert(_done);

  return true;
}

const CType* CAstArrayDesignator::GetType(void) const
{
  const CType *result = GetSymbol()->GetDataType();
  // derefer if it is pointer
  if (result->IsPointer())
    result = dynamic_cast<const CPointerType*>(result)->GetBaseType();
  // result type should be array 
  if (!result->IsArray())
    return NULL;
  // number of indices <= number of dimensions
  if (GetNIndices() > dynamic_cast<const CArrayType*>(result)->GetNDim())
    return NULL;
  for (int i = 0; i < GetNIndices(); ++i)
    // repeatedly get inner type
    result = dynamic_cast<const CArrayType*>(result)->GetInnerType();
  return result;
}

ostream& CAstArrayDesignator::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << _symbol << " ";

  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";

  out << endl;

  for (size_t i=0; i<_idx.size(); i++) {
    _idx[i]->print(out, indent+2);
  }

  return out;
}

string CAstArrayDesignator::dotAttr(void) const
{
  ostringstream out;
  out << " [label=\"" << _symbol->GetName() << "[]\",shape=ellipse]";
  return out.str();
}

void CAstArrayDesignator::toDot(ostream &out, int indent) const
{
  string ind(indent, ' ');

  CAstNode::toDot(out, indent);

  for (size_t i=0; i<_idx.size(); i++) {
    _idx[i]->toDot(out, indent);
    out << ind << dotID() << "-> " << _idx[i]->dotID() << ";" << endl;
  }
}

CTacAddr* CAstArrayDesignator::ToTac(CCodeBlock *cb)
{
  assert(cb != NULL);

  CTypeManager *tm = CTypeManager::Get();
  CSymtab *st = GetSymbol()->GetSymbolTable();
  CTacName *dim = new CTacName(st->FindSymbol("DIM"));
  CTacName *dofs = new CTacName(st->FindSymbol("DOFS"));
  const CArrayType *arrtype = dynamic_cast<const CArrayType*>(GetType());

  // &A
  CTacName *base, *tmp;
  if (GetSymbol()->GetDataType()->IsPointer()) {
    base = new CTacName(GetSymbol());
  } else {
    base = cb->CreateTemp(tm->GetPointer(GetSymbol()->GetDataType()));
    cb->AddInstr(new CTacInstr(opAddress, base, new CTacName(GetSymbol())));
  }

  // calculate offset; row major ordering
  CTacAddr *idx, *n, *size;
  idx = GetIndex(0)->ToTac(cb);
  int N = GetNIndices();  // count indices in the designator
  if (arrtype != NULL) N += arrtype->GetNDim();  // add remaining dimension
  for (int i=1; i<N; ++i) {
    cb->AddInstr(new CTacInstr(opParam, new CTacConst(1), new CTacConst(i+1)));
    if (GetSymbol()->GetDataType()->IsPointer()) {
      tmp = new CTacName(GetSymbol());
    } else {
      tmp = cb->CreateTemp(tm->GetPointer(GetSymbol()->GetDataType()));
      cb->AddInstr(new CTacInstr(opAddress, tmp, new CTacName(GetSymbol())));
    }
    cb->AddInstr(new CTacInstr(opParam, new CTacConst(0), tmp));
    size = cb->CreateTemp(tm->GetInt());
    cb->AddInstr(new CTacInstr(opCall, size, dim));

    tmp = cb->CreateTemp(tm->GetInt());
    cb->AddInstr(new CTacInstr(opMul, tmp, idx, size));
    if (i < GetNIndices())
      n = GetIndex(i)->ToTac(cb);
    else
      n = new CTacConst(0);  // implicit arguments to get correct address
    idx = cb->CreateTemp(tm->GetInt());
    cb->AddInstr(new CTacInstr(opAdd, idx, tmp, n));
  }

  // int : 4 byte / char & boolean : 1 byte
  CTacName *offset = cb->CreateTemp(tm->GetInt());
  int l;
  if (arrtype != NULL)
    l = arrtype->GetBaseType()->IsInt() ? 4 : 1;
  else
    l = GetType()->IsInt() ? 4 : 1;
  cb->AddInstr(new CTacInstr(opMul, offset, idx, new CTacConst(l)));

  // DOFS(A)
  if (GetSymbol()->GetDataType()->IsPointer()) {
    tmp = new CTacName(GetSymbol());
  } else {
    tmp = cb->CreateTemp(tm->GetPointer(GetSymbol()->GetDataType()));
    cb->AddInstr(new CTacInstr(opAddress, tmp, new CTacName(GetSymbol())));
  }
  cb->AddInstr(new CTacInstr(opParam, new CTacConst(0), tmp));
  CTacName *firstA = cb->CreateTemp(tm->GetInt());
  cb->AddInstr(new CTacInstr(opCall, firstA, dofs));

  // result = &A + (offset + DOFS(A))
  CTacName *bias = cb->CreateTemp(tm->GetInt());
  cb->AddInstr(new CTacInstr(opAdd, bias, offset, firstA));
  CTacName *result = cb->CreateTemp(tm->GetInt());
  cb->AddInstr(new CTacInstr(opAdd, result, base, bias));

  return new CTacReference(result->GetSymbol(), GetSymbol());
}

CTacAddr* CAstArrayDesignator::ToTac(CCodeBlock *cb,
                                     CTacLabel *ltrue, CTacLabel *lfalse)
{
  assert(cb != NULL && ltrue != NULL && lfalse != NULL);
  assert(GetType()->IsBoolean());

  CTacAddr* tmp = ToTac(cb);
  cb->AddInstr(new CTacInstr(opEqual, ltrue, tmp, new CTacConst(1)));
  cb->AddInstr(new CTacInstr(opGoto, lfalse));

  return NULL;
}


//------------------------------------------------------------------------------
// CAstConstant
//
CAstConstant::CAstConstant(CToken t, const CType *type, long long value)
  : CAstOperand(t), _type(type), _value(value)
{
}

void CAstConstant::SetValue(long long value)
{
  _value = value;
}

long long CAstConstant::GetValue(void) const
{
  return _value;
}

string CAstConstant::GetValueStr(void) const
{
  ostringstream out;

  if (GetType() == CTypeManager::Get()->GetBool()) {
    out << (_value == 0 ? "false" : "true");
  } else {
    out << dec << _value;
  }

  return out.str();
}

bool CAstConstant::TypeCheck(CToken *t, string *msg) const
{
  // range check for integers
  if (_type->IsInt() &&
      (_value >= ((long long)1 << 31) || _value < -((long long)1 << 31))) {
    *t = GetToken();
    *msg = "integer constant outside valid range.";
    return false;
  }
  else return true;
}

const CType* CAstConstant::GetType(void) const
{
  return _type;
}

ostream& CAstConstant::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << GetValueStr() << " ";

  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";

  out << endl;

  return out;
}

string CAstConstant::dotAttr(void) const
{
  ostringstream out;
  out << " [label=\"" << GetValueStr() << "\",shape=ellipse]";
  return out.str();
}

CTacAddr* CAstConstant::ToTac(CCodeBlock *cb)
{
  assert(cb != NULL);

  return new CTacConst((int)GetValue());
}

CTacAddr* CAstConstant::ToTac(CCodeBlock *cb,
                                CTacLabel *ltrue, CTacLabel *lfalse)
{
  assert(cb != NULL && ltrue != NULL && lfalse != NULL);
  assert(GetType()->IsBoolean());

  // since the value is fixed, can eliminate condition
  cb->AddInstr(new CTacInstr(opGoto, GetValue() ? ltrue : lfalse));

  return NULL;
}


//------------------------------------------------------------------------------
// CAstStringConstant
//
int CAstStringConstant::_idx = 0;

CAstStringConstant::CAstStringConstant(CToken t, const string value,
                                       CAstScope *s)
  : CAstOperand(t)
{
  CTypeManager *tm = CTypeManager::Get();

  _type = tm->GetArray(strlen(CToken::unescape(value).c_str())+1,
                       tm->GetChar());
  _value = new CDataInitString(value);

  ostringstream o;
  o << "_str_" << ++_idx;

  _sym = new CSymGlobal(o.str(), _type);
  _sym->SetData(_value);
  s->GetSymbolTable()->AddSymbol(_sym);
}

const string CAstStringConstant::GetValue(void) const
{
  return _value->GetData();
}

const string CAstStringConstant::GetValueStr(void) const
{
  return GetValue();
}

bool CAstStringConstant::TypeCheck(CToken *t, string *msg) const
{
  // no way to make invalid string if scanner is sound
  return true;
}

const CType* CAstStringConstant::GetType(void) const
{
  return _type;
}

ostream& CAstStringConstant::print(ostream &out, int indent) const
{
  string ind(indent, ' ');

  out << ind << '"' << GetValueStr() << '"' << " ";

  const CType *t = GetType();
  if (t != NULL) out << t; else out << "<INVALID>";

  out << endl;

  return out;
}

string CAstStringConstant::dotAttr(void) const
{
  ostringstream out;
  // the string is already escaped, but dot requires double escaping
  out << " [label=\"\\\"" << CToken::escape(GetValueStr())
      << "\\\"\",shape=ellipse]";
  return out.str();
}

CTacAddr* CAstStringConstant::ToTac(CCodeBlock *cb)
{
  assert(cb != NULL);

  return new CTacName(_sym);
}

CTacAddr* CAstStringConstant::ToTac(CCodeBlock *cb,
                                CTacLabel *ltrue, CTacLabel *lfalse)
{
  assert(GetType()->IsBoolean());  // will always fail

  return NULL;
}


