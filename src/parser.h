//------------------------------------------------------------------------------
/// @brief SnuPL/0 parser
/// @author Bernhard Egger <bernhard@csap.snu.ac.kr>
/// @section changelog Change Log
/// 2012/09/14 Bernhard Egger created
/// 2013/03/07 Bernhard Egger adapted to SnuPL/0
/// 2016/03/09 Bernhard Egger adapted to SnuPL/!
/// 2016/04/08 Bernhard Egger assignment 2: parser for SnuPL/-1
///
/// @section license_section License
/// Copyright (c) 2012-2016, Bernhard Egger
/// All rights reserved.
///
/// Redistribution and use in source and binary forms,  with or without modifi-
/// cation, are permitted provided that the following conditions are met:
///
/// - Redistributions of source code must retain the above copyright notice,
///   this list of conditions and the following disclaimer.
/// - Redistributions in binary form must reproduce the above copyright notice,
///   this list of conditions and the following disclaimer in the documentation
///   and/or other materials provided with the distribution.
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
/// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
/// IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
/// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER  OR CONTRIBUTORS BE
/// LIABLE FOR ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSE-
/// QUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF  SUBSTITUTE
/// GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
/// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT
/// LIABILITY, OR TORT  (INCLUDING NEGLIGENCE OR OTHERWISE)  ARISING IN ANY WAY
/// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
/// DAMAGE.
//------------------------------------------------------------------------------

#ifndef __SnuPL_PARSER_H__
#define __SnuPL_PARSER_H__

#include "scanner.h"
#include "symtab.h"
#include "ast.h"


//------------------------------------------------------------------------------
/// @brief parser
///
/// parses a module
///
class CParser {
  public:
    /// @brief constructor
    ///
    /// @param scanner  CScanner from which the input stream is read
    CParser(CScanner *scanner);

    /// @brief parse a module
    /// @retval CAstNode program node
    CAstNode* Parse(void);

    /// @name error handling
    ///@{

    /// @brief indicates whether there was an error while parsing the source
    /// @retval true if the parser detected an error
    /// @retval false otherwise
    bool HasError(void) const { return _abort; };

    /// @brief returns the token that caused the error
    /// @retval CToken containing the error token
    const CToken* GetErrorToken(void) const;

    /// @brief returns a human-readable error message
    /// @retval error message
    string GetErrorMessage(void) const;
    ///@}

  private:
    /// @brief sets the token causing a parse error along with a message
    /// @param t token causing the error
    /// @param message human-readable error message
    void SetError(CToken t, const string message);

    /// @brief consume a token given type and optionally store the token
    /// @param type expected token type
    /// @param token If not null, the consumed token is stored in 'token'
    /// @retval true if a token has been consumed
    /// @retval false otherwise
    bool Consume(EToken type, CToken *token=NULL);


    /// @brief initialize symbol table @a s with predefined procedures and
    ///        global variables
    void InitSymbolTable(CSymtab *s);

    /// @name methods for recursive-descent parsing
    /// @{

    // module
    /// @brief generate ast module node from token stream
    CAstModule*         module(void);

    // declarations
    /// @brief generate ast type node from token stream
    /// @param isParam check whether parsing variable or parameters
    CAstType*           type(bool isParam);
    /// @brief parse and store symbols to symbol table and/or function symbol
    /// @param s scope of variables or parameters
    /// @param isParam check whether parsing variable or parameters
    /// @param idx first index of variable
    /// @retval number of parsed variables
    int                varDecl(CAstScope *s, bool isParam, int idx);
    /// @brief parsing variables
    /// @param s scope of variables
    void                varDeclaration(CAstScope *s);
    /// @brief parsing function parameters
    /// @param s scope of parameters
    void                paramDeclaration(CAstScope *s);
    /// @brief generate ast subroutine node from token stream
    /// @param s scope of parsed subroutine
    CAstProcedure*      subroutineDeclaration(CAstScope *s);

    // statements
    /// @brief generate ast assign statement node from token stream
    /// @param s scope of parsed assign statement
    /// @param ident first token of expecting assign statement
    CAstStatAssign*     assignStatement(CAstScope *s, CToken ident);
    /// @brief generate ast call statement node from token stream
    /// @param s scope of parsed call statement
    /// @param ident first token of expecting call statement
    CAstStatCall*       callStatement(CAstScope *s, CToken ident);
    /// @brief generate ast if statement node from token stream
    /// @param s scope of parsed if statement
    CAstStatIf*         ifStatement(CAstScope *s);
    /// @brief generate ast while statement node from token stream
    /// @param s scope of parsed while statement
    CAstStatWhile*      whileStatement(CAstScope *s);
    /// @brief generate ast return statement node from token stream
    /// @param s scope of parsed return statement
    CAstStatReturn*     returnStatement(CAstScope *s);
    /// @brief generate ast statement node from token stream
    /// @param s scope of parsed statement
    CAstStatement*      statement(CAstScope *s);
    /// @brief generate list of ast statements from token stream
    /// @param s scope of parsed statement sequence
    CAstStatement*      statSequence(CAstScope *s);

    // expressions
    /// @brief generate a ast designator from token stream
    /// @param s scope of parsed designator
    /// @param ident first token of expecting designator
    CAstDesignator*     qualident(CAstScope *s, CToken ident);
    /// @brief generate a ast subroutine call from token stream
    /// @param s scope of parsed subroutine call
    /// @param ident first token of expecting subroutine call
    CAstFunctionCall*   subroutineCall(CAstScope *s, CToken ident);
    /// @brief generate a factor from token stream
    /// @param s scope of parsed expression
    CAstExpression*     factor(CAstScope *s);
    /// @brief generate a term from token stream
    /// @param s scope of parsed expression
    CAstExpression*     term(CAstScope *s);
    /// @brief generate a simpleexpr from token stream
    /// @param s scope of parsed expression
    CAstExpression*     simpleexpr(CAstScope *s);
    /// @brief generate a ast expression from token stream
    /// @param s scope of parsed expression
    CAstExpression*     expression(CAstScope *s);

    // constants
    /// @brief generate a integer literal from token stream
    CAstConstant*       constNumber(void);
    /// @brief generate a boolean literal from token stream
    CAstConstant*       constBoolean(void);
    /// @brief generate a character literal from token stream
    CAstConstant*       constChar(void);
    /// @brief generate a string literal from token stream
    /// @param s scope that store parsed string constant
    CAstStringConstant* constString(CAstScope *s);
    /// @}


    CScanner     *_scanner;       ///< CScanner instance
    CAstModule   *_module;        ///< root node of the program
    CToken        _token;         ///< current token

    /// @name error handling
    CToken        _error_token;   ///< error token
    string        _message;       ///< error message
    bool          _abort;         ///< error flag

};

#endif // __SnuPL_PARSER_H__
