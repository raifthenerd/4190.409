CXXFLAGS=-std=c++11 -O3

SRC_DIR=src
OBJ_DIR=obj

DEPS=scanner.h \
     parser.h \
     type.h \
     data.h \
     symtab.h \
     ast.h \
     ir.h \
     backend.h
SCANNER=scanner.cpp
PARSER=parser.cpp \
       type.cpp \
       data.cpp \
       symtab.cpp \
       ast.cpp \
       ir.cpp
IR=
BACKEND=backend.cpp

DEPS_=$(patsubst %,$(SRC_DIR)/%,$(DEPS))
OBJ_SCANNER=$(patsubst %.cpp,$(OBJ_DIR)/%.o,$(SCANNER))
OBJ_PARSER=$(patsubst %.cpp,$(OBJ_DIR)/%.o,$(PARSER))
OBJ_IR=$(patsubst %.cpp,$(OBJ_DIR)/%.o,$(IR) $(PARSER))
OBJ_SNUPLC=$(patsubst %.cpp,$(OBJ_DIR)/%.o,$(BACKEND) $(IR) $(PARSER))

.PHONY: clean doc test

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(DEPS_)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

all: snuplc

test: snuplc
	@test/script.sh snuplc

test_scanner: $(OBJ_DIR)/test_scanner.o
	$(CXX) $(CXXFLAGS) -o $@ $(OBJ_DIR)/test_scanner.o $(OBJ_SCANNER)

test_semantic: $(OBJ_DIR)/test_parser.o $(OBJ_PARSER)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJ_DIR)/test_parser.o $(OBJ_SCANNER) $(OBJ_PARSER)

test_ir: $(OBJ_DIR)/test_ir.o $(OBJ_IR)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJ_DIR)/test_ir.o $(OBJ_SCANNER) $(OBJ_IR)

snuplc: $(OBJ_DIR)/snuplc.o $(OBJ_SNUPLC)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJ_DIR)/snuplc.o $(OBJ_SCANNER) $(OBJ_SNUPLC)

doc:
	doxygen

clean:
	rm -rf $(OBJ_DIR)/*.o test_scanner test_semantic test_ir snuplc
	cp ref/obj/* obj/

mrproper: clean
	rm -rf doc/*
