# SnuPL/1 Compiler
[![Build Status](https://travis-ci.com/raifthenerd/SnuPL1.svg?token=xWPjX4c8rH9et1Aydxm7&branch=stable)](https://travis-ci.com/raifthenerd/SnuPL1)

2016-1 4190.409 Compilers (SNU) Term Project, tested with g++ 4.8.

## Authors
* Seokjin Han (2012-11853)
* Seonghak Kim (2012-13365)
